<?php



function create_custom_contact_form_shortcode()
{
    $url = esc_url( admin_url('admin-post.php') );
    $out = '';
    $out .= '<h3 class="text-capitalize text-center ">Send your message</h3>';

    $out .= "<form method='post' id='custom-contact-form' class='position-relative container' action='$url'>";
    $out .= '<input type="text" name="subject" id="subject" placeholder="enter subject of message">';
    $out .= '<h4 class="w-100 d-flex text-center justify-content-center my-3">message</h4>';
    $out .= '<textarea name="message" id="message"></textarea>';
    $out .= '<input type="hidden" name="action" value="contact_form">';
    if (is_user_logged_in())
    {
        $out .= '<button name="send" class="my-3 rounded" id="send-contact-form" type="submit">Send</button>';
    }else{
        $login_link = wp_login_url();
        $out .= "<span class='my-3 font-weight-bold'>To send messages you must be <a href='$login_link'>logged in</a></span>";
    }

    $out .= '</form>';
    return $out;
}


add_shortcode('custom-contact-form', 'create_custom_contact_form_shortcode');

function send_test_mail()
{
    if (isset($_POST['subject']) && isset($_POST['message']))
    {
        $subject = $_POST['subject'];
        $message = $_POST['message'];
        $user = _wp_get_current_user();
        $from_email = $user->data->user_email;
        $from_name = $user->data->user_nicename;
        $headers = "From: $from_name <$from_email>" . "\r\n";
        return  wp_mail('didjok6@gmail.com', $subject, $message, $headers);
    }
}

 add_action('admin_post_contact_form', 'send_test_mail');


add_filter( 'widget_text', 'do_shortcode' );

