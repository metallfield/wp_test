<?php

add_action('add_meta_boxes', 'custom_meta_boxes');

function custom_meta_boxes()
{
    // is_has_slider_metabox
    $screens = ['page', 'animals'];
    add_meta_box('has_slider_metabox', 'Has Slider', 'has_slider_metabox_callback', $screens);

    // is_archive_page

    $screens = ['page'];
    add_meta_box('is_archive_page_template', 'Is Archive Page', 'is_archive_page_metabox_callback', $screens);

    $screens = ['page'];
    add_meta_box('archive_posts_layout', 'Set posts output view', 'archive_posts_layout', $screens);

}

function has_slider_metabox_callback($post, $meta)
{
    $screens = $meta['args'];

    // Используем nonce для верификации
    wp_nonce_field( plugin_basename(__FILE__), 'myplugin_noncename' );

    // значение поля
    $value = get_post_meta( $post->ID, 'has_slider', 1 );
    $has_slider = $value == 1 ? 'checked' : '';
    $dont_has_slider = $value == 0 ? 'checked' : '';
    // Поля формы для введения данных
    echo '<label for="has_slider_metabox">' . __("yes", 'myplugin_textdomain' ) . '</label> ';
    echo '<input type="radio" id="has_slider_metabox" name="has_slider_metabox" value="1" size="25" '.$has_slider.'  ><br>';
    echo '<label for="has_slider_metabox">' . __("no", 'myplugin_textdomain' ) . '</label> ';
    echo '<input type="radio" id="has_slider_metabox" name="has_slider_metabox" '. $dont_has_slider.' value="0" size="25" >';
}

add_action( 'save_post', 'has_slider_metabox_save_postdata' );
function has_slider_metabox_save_postdata( $post_id ) {
    if ( ! isset( $_POST['has_slider_metabox'] ) )
        return;

    if ( ! wp_verify_nonce( $_POST['myplugin_noncename'], plugin_basename(__FILE__) ) )
        return;


    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
        return;


    if( ! current_user_can( 'edit_post', $post_id ) )
        return;

    $my_data = $_POST['has_slider_metabox'];

    update_post_meta( $post_id, 'has_slider', $my_data );
}

function is_archive_page_metabox_callback($post, $meta)
{
    $screens = $meta['args'];

    // Используем nonce для верификации
    wp_nonce_field( plugin_basename(__FILE__), 'myplugin_noncename' );

    // значение поля
    $value = get_post_meta( $post->ID, 'is_archive_page_template', 1 )
        ? get_post_meta( $post->ID, 'is_archive_page_template', 1 )
        : 'false' ;
    $selected_yes = $value == 'true' ? 'selected' : '';
    $selected_no = $value == 'false' ? 'selected' : '';
    echo '<label for="is_archive">'.__('Is it archive page ?', 'myplugin_textdomain').'</label><br>';
    echo '<select id="is_archive" value="false"  name="is_archive">
            <option value="true" '.$selected_yes.'>'.__('yes', 'myplugin_textdomain').'</option>
            <option value="false"  '.$selected_no.'>'.__('no', 'myplagin_textdomain').'</option>
            </select>';

    //  set post type for archive
    $current_post_type = get_post_meta($post->ID, 'archive_page_post_type', true);
    $post_types = get_post_types(['_builtin' => false], 'names');
    echo '<div hidden id="admin_post_types_filter">
           <label for="posts_type">'.__('select archive posts type', 'myplagin_textdomain').'</label><br>
           <select name="posts_type" id="posts_type">
           ';

    function is_type_selected($post_type, $current_post_type)
    {
        echo $post_type == $current_post_type ? 'selected' : '';
    }
    foreach ($post_types as $post_type)
    {
        echo '<option value="' . $post_type.'" ';
        is_type_selected($post_type, $current_post_type);
        echo '>'.$post_type.'</option>';
    };
    echo '</select>
</div>';

}

add_action( 'save_post', 'save_is_archive_page_metabox_value' );

function save_is_archive_page_metabox_value( $post_id )
{
    if ( ! isset( $_POST['is_archive'] ) )
        return;

    if ( ! wp_verify_nonce( $_POST['myplugin_noncename'], plugin_basename(__FILE__) ) )
        return;


    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
        return;


    if( ! current_user_can( 'edit_post', $post_id ) )
        return;

    $my_data = $_POST['is_archive'];
    if ($my_data == 'true')
    {
        $archive_post_type = $_POST['posts_type'];
        update_post_meta($post_id, 'archive_page_post_type', $archive_post_type);
    }else{
        update_post_meta($post_id, 'archive_page_post_type', null);
    }
    update_post_meta( $post_id, 'is_archive_page_template', $my_data );
}

function archive_posts_layout($post, $meta)
{
    $screens = $meta['args'];

    // Используем nonce для верификации
    wp_nonce_field( plugin_basename(__FILE__), 'myplugin_noncename' );

    // значение поля
    $value = get_post_meta( $post->ID, 'archive_posts_layout', 1 );
    $grid_selected = $value == 'grid' ? 'selected': '';
    $line_selected = $value == 'line' ? 'selected': '';
    echo '<div id="archive_posts_layout"  >
           <select name="posts_layout" id="posts_layout">
           <option value="line" '.$line_selected.'>line</option>
           <option value="grid" '.$grid_selected.'>grid</option>
</select></div>
    ';
}

add_action('save_post', 'save_archive_posts_layout');

function save_archive_posts_layout($post_id)
{
    if ( ! isset( $_POST['posts_layout'] ) )
        return;

    if ( ! wp_verify_nonce( $_POST['myplugin_noncename'], plugin_basename(__FILE__) ) )
        return;


    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
        return;


    if( ! current_user_can( 'edit_post', $post_id ) )
        return;

    $my_data = $_POST['posts_layout'];
    $is_archive_page = $_POST['is_archive'];
    if ($is_archive_page)
    {
        update_post_meta($post_id, 'archive_posts_layout', $my_data);
    }else{
        update_post_meta($post_id, 'archive_posts_layout', $my_data);
    }
}