<?php
new Test_Block(); // инициализация

class Test_Block {

    public function __construct() {
        add_action( 'init', [ $this, 'gutenberg_register_block' ] );
        add_action( 'enqueue_block_editor_assets', [ $this, 'gutenberg_editor_scripts' ] );
    }

    public function gutenberg_register_block() {
        register_block_type( 'gutenberg-block/123', [
            'attributes'      => [
                'postsPerPage' => [
                    'type'    => 'number',
                    'default' => 3,
                ],
                'order'        => [
                    'type'    => 'string',
                    'default' => 'desc',
                ],
            ],
            'render_callback' => [ $this, 'gutenberg_render_callback' ]
        ] );
    }

    public function gutenberg_editor_scripts() {
        wp_register_script(
            'test_handle',
            get_stylesheet_directory_uri().'/blocks/test_block/build/index.js',
            ['wp-blocks']
        );

        wp_enqueue_script( 'test_handle' );

    }

    public function gutenberg_render_callback( $attributes, $content ) {

        $args = [
            'posts_per_page' => $attributes['postsPerPage'],
            'post_status'    => 'publish',
            'order'          => $attributes['order'],
        ];

        $posts = get_posts( $args );

        $html = '<div>';
        if ( $posts ) {
            $html .= '<ul>';

            foreach ( $posts as $item ) {
                $html .= '<li><a href="' . get_the_permalink( $item->ID ) . '">' . $item->post_title . '</a></li>';
            }

            $html .= '</ul>';
        } else {
            $html .= '<h3>' . __( 'No posts!', 'gutenberg-blocks' ) . '</h3>';
        }

        $html .= '</div>';

        return $html;

    }

}