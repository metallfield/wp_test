<?php

function gutenberg_examples_dynamic_render_callback( $attributes ) {
     $type = $attributes['postType'];
    $count = $attributes['postCount'];
    $excerpt_alignment = $attributes['contentAlignment'];
    $title_color = $attributes['titleColor'];
    $excerpt_color = $attributes['descriptionColor'];
    $posts_margin = $attributes['range_attr'].'px';


    $posts = query_posts(['post_type' => $type, 'posts_per_page'=> $count]);
    $html = '';
    foreach ($posts as $post)
    {
        $link = get_post_permalink($post->ID);
        $html .= "<div style='margin: $posts_margin '>";
        $html .= "<h4 class='text-capitalize text-center' > <a href='$link' class='my-3' style='color: $title_color'>$post->post_title</a></h4>";
        $html .= "<span class='text-center mx-auto w-100 d-flex justify-content-center text-secondary font-italic '>$post->post_date</span>";
        if (isset($post->post_excerpt))
        {
            $html .= "<p class=' p-2 text-$excerpt_alignment'  style='color: $excerpt_color'>$post->post_excerpt</p><hr>";
        }
        $html .= "</div>";
    }
    wp_reset_query();
      return $html;
}

function gutenberg_examples_dynamic() {
    // automatically load dependencies and version
     $path = get_stylesheet_directory_uri();

    wp_register_script(
        'gutenberg-examples-dynamic',
         get_stylesheet_directory_uri().'/blocks/dynamyc_block/build/index.js'
      );
    $posts = query_posts(['post_type' => 'post', 'posts_per_page' => 3]);
    wp_reset_query();
    register_block_type( 'gutenberg-examples/example-dynamic', array(
        'editor_script' => 'gutenberg-examples-dynamic',
        'render_callback' => 'gutenberg_examples_dynamic_render_callback',

    ) );

}
add_action( 'init', 'gutenberg_examples_dynamic' );