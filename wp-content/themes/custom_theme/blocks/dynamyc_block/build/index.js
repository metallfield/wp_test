( function( blocks, element,  editor, components, data) {
    var el = element.createElement,
        registerBlockType = blocks.registerBlockType;
    const {withSelect, select} = data;
    var AlignmentToolbar = editor.AlignmentToolbar;
    const {RichText, InspectorControls, BlockControls} = editor;
    const { TextControl, ToggleControl, Panel, PanelBody, PanelRow, CheckboxControl, SelectControl, TextareaControl, RangeControl, RadioControl, ColorPicker } = components;

     registerBlockType( 'gutenberg-examples/example-dynamic', {
        title: 'Example: last post',
        icon: 'megaphone',
        category: 'widgets',
        attributes: {
            postType: {
                type: 'string'
            },
            postCount: {
                type: 'string'
            },
            range_attr: {
                type: 'number'
            },
            titleColor: {
                type: 'string'
            },
            descriptionColor: {
                type: 'string'
            },
            contentAlignment: {
                type: 'string'
            }
        },
         example: {
            attributes: {
                content: 'Hello World',
                alignment: 'right',
            },
        },
        edit: withSelect((select, props) => {
            const {getPostTypes} = select('core');
             return  {
                posts : select('core').getEntityRecords('postType', props.attributes.postType ?? 'post', {per_page : props.attributes.postCount ?? 3}) ??
                    select('core').getEntityRecords('postType', props.attributes.postType ?? 'post', {per_page : -1}),
                 posts_types: getPostTypes()
            }
        }) ( function( props ) {
            console.log(props)

            var types = []
            props.posts_types !== null ?
            props.posts_types.map((type)=>{
                let without = ['Media', 'Pages', 'Blocks'];
                ! without.includes(type.name) ?
                types.push({label: type.name, value : type.slug } ) : null
            }): '' ;
            console.log(types);
            return [  el( InspectorControls, {},
                    el( PanelBody, { title: 'Custom Form Settings', initialOpen: true },

                        el(PanelRow, {},
                            // Select dropdown field
                            el( SelectControl,
                                {
                                    label: 'Select Posts Type',
                                    options : types ,
                                    onChange: ( value ) => {
                                        props.setAttributes( { postType: value } );
                                    },
                                    value: props.attributes.postType
                                }
                            ),
                            ),
                        /* Text Field */
                        el( PanelRow, {},
                            el( TextControl,
                                {
                                    label: 'Enter Count Of Posts',
                                    onChange: ( value ) => {
                                        props.setAttributes( { postCount: value } );
                                    },
                                    value: props.attributes.postCount
                                }
                            ),

                        ),
                        el( PanelRow, {},
                                el(
                                    ColorPicker,
                                    {
                                        label: 'Select Posts Title Color',
                                        color: props.attributes.titleColor,
                                        disabledAlpha: true,
                                        onChangeComplete: (value) => {
                                            console.log(value)
                                            props.setAttributes({titleColor: value.hex})
                                        }
                                    }
                                )
                            ),
                        el( PanelRow, {},
                                el(
                                    ColorPicker,
                                    {
                                        label: 'Select Posts Description Color',
                                        color: props.attributes.descriptionColor,
                                        disabledAlpha: true,
                                        onChangeComplete: (value) => {
                                            console.log(value)
                                            props.setAttributes({descriptionColor: value.hex})
                                        }
                                    }
                                )
                            ),
                        el(
                            PanelRow, {},
                            el(
                                RichText,
                                {
                                    label: 'test rich text',
                                    value: props.attributes.rich_attr,
                                    placeholder: 'test rich text',
                                    multiple: "li",
                                    onChange: (valaue) => {
                                        props.setAttributes({rich_attr: valaue});
                                    }
                                }
                            )
                        ),
                        el(
                            PanelRow, {},
                            // Range
                            el( RangeControl,
                                {
                                    label: 'Range Around the Posts',
                                    min: 0,
                                    max: 100,
                                    onChange: ( value ) => {
                                        props.setAttributes( { range_attr: value } );
                                    },
                                    value: props.attributes.range_attr
                                }
                            ),
                        ),
                         /* Toggle Field */
                        el( PanelRow, {},
                            el( ToggleControl,
                                {
                                    label: 'Double Opt In',
                                    onChange: ( value ) => {
                                        props.setAttributes( { doubleoptin: value } );
                                    },
                                    checked: props.attributes.doubleoptin,
                                }
                            )
                        )

                    ),
                    el( PanelBody, { title: 'Custom Awesome fields', initialOpen: false },

                        // Textarea field
                        el( TextareaControl,
                            {
                                label: 'Textarea Control',
                                onChange: ( value ) => {
                                    props.setAttributes( { textarea_attr: value } );
                                },
                                value: props.attributes.textarea_attr,
                            }
                        ),

                        // Checkbox field
                        el( CheckboxControl,
                            {
                                label: 'Checkbox Control',
                                onChange: ( value ) => {
                                    props.setAttributes( { chekbox_attr: value } );
                                },
                                checked: props.attributes.chekbox_attr,
                            }
                        ),



                        // Radio buttons
                        el( RadioControl,
                            {
                                label: 'Radio Control',
                                //help: 'Some kind of description',
                                options : [
                                    { label: 'Option 1', value: 'value_1' },
                                    { label: 'Option 2', value: 'value_2' },
                                ],
                                onChange: ( value ) => {
                                    props.setAttributes( { radio_attr: value } );
                                },
                                selected: props.attributes.radio_attr
                            }
                        ),


                    )
                ),

                    props.posts !== null ?
                    el(
                        'div',
                        null,
                        el(
                            BlockControls,
                            {
                                key: 'controls'
                            },
                            el(
                                AlignmentToolbar,
                                {
                                    value: props.attributes.contentAlignment,
                                    onChange: (value) => {
                                        props.setAttributes({contentAlignment: value ?? 'none'});
                                    }
                                }
                            )
                        ),
                        props.posts.map((item)=>{
                            return [
                                el(
                                'div',
                                    {style :{display: 'flex', flexDirection: 'column', margin: props.attributes.range_attr}},
                                el(
                                    'h3',
                                    {style:{textAlign: 'center'}},
                                    el(
                                        'a',
                                        {href: item.link, style: {color: props.attributes.titleColor}},
                                        item.title.rendered
                                    )
                                ),
                                el(
                                    'span',
                                    {style: { textAlign : props.attributes.contentAlignment, color: props.attributes.descriptionColor }},
                                    item.excerpt.raw
                                )
                            )]

                        })
                    ) : 'no posts with this params'
                ];


        }),
        save: function (props) {
            return null;
        }
    } );
}(
    window.wp.blocks,
    window.wp.element,
     window.wp.editor,
    window.wp.components,
    window.wp.data,
) );