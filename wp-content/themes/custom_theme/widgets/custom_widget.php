<?php

if( ! defined('ABSPATH') ) exit;


add_action('widgets_init', 'register_map_custom_widget');

function register_map_custom_widget()
{
    register_widget('Custom_Map_Widget');
}


class Custom_Map_Widget extends WP_Widget{
    function __construct()
    {
        parent::__construct('custom_map_widget', 'Map Custom Widget', ['description' => 'trying create custom widget']);
        if ( is_active_widget( false, false, $this->id_base ) || is_customize_preview() ) {
            add_action('wp_enqueue_scripts', array( $this, 'add_my_widget_scripts' ));
         }
    }

    public function widget($args, $instance)
    {
         ?>
        <h3><?php echo  $instance['title'];?></h3>
        <div class="form-group">
             <input type="text" hidden id="address-input" name="address_address" class="form-control map-input" value="">
            <input type="hidden" name="address_latitude" id="address-latitude"  value="<?php echo $instance['map_lat'];?>" />
            <input type="hidden" name="address_longitude" id="address-longitude"  value="<?php echo $instance['map_lng'];?>" />
        </div>
        <div id="address-map-container" style="width:100%; height:400px; ">
            <div style="width: 100%; height: 100%" id="address-map" name="address-map"></div>
        </div>
        <?php
    }
    public function form($instance)
    {
        ?>
        <h3>title</h3>
        <input type="text" id="<?php echo $this->get_field_id('title');?>" name="<?php echo $this->get_field_name('title');?>" placeholder="enter title" value="<?php echo $instance['title'];?>">
        <hr>
        <div>
            <h3 >latitude</h3>
            <input type="text" id="<?php echo  $this->get_field_id('map_lat');?>" name="<?php echo $this->get_field_name('map_lat');?>" value="<?php echo $instance['map_lat']?> ">
        </div>
        <hr>
        <div>
            <h3 >longitude</h3>
            <input type="text" id="<?php echo $this->get_field_id('map_lng');?>" name="<?php echo $this->get_field_name('map_lng');?>" value="<?php echo $instance['map_lng']?>">
        </div>

        <?php
    }

    public function update($new_instance, $old_instance)
    {
        $instance = [];
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? ( $new_instance['title'] ) : '';
        $instance['map_lat'] = ! empty($new_instance['map_lat']) ? $new_instance['map_lat'] : '';
        $instance['map_lng'] = ! empty($new_instance['map_lng']) ? $new_instance['map_lng'] : '';
        return $instance;
    }

    public function add_my_widget_scripts()
     {
         if( ! apply_filters( 'show_my_widget_script', true, $this->id_base ) )
             return;
         wp_enqueue_script('google-map-script', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyA3tD5rPn0Tkd8ool8kd1lezWCL72DdWbs&libraries=places&callback=initialize', ['google-map-init'], null, true );
         wp_enqueue_script('google-map-init', get_stylesheet_directory_uri().'/assets/js/google-maps-init.js', ['jquery'], null, true);

     }
}

