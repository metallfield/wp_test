$(document).ready(()=>{
    $('#custom-contact-form').submit((e)=>{

         let action = $('#custom-contact-form').attr('action');
         let data = {
             action: 'contact_form',
             message: $('#message').val(),
             subject: $('#subject').val()
         }
         console.log(data)
         $.post(action, data,  (response)=>{
            console.log(response);
        }).success((data)=>{
             console.log('success');
             let info = '<div class=" w-100 bg-info border rounded p-3 text-white my-2" id="info">message successfully sent</div>'
             $('#custom-contact-form').prepend(info);

             setTimeout(()=>{
                 $('#info').remove()
             }, 2000);
         }).error((error)=>{
             console.log('error')
         })
        e.preventDefault();
   })
});