$(document).ready(()=>{
    wp.customize( 'display_header', function( value ) {
        value.bind( function( newVal ) {
            false === newVal ? $( '#archive-title' ).hide() : $( '#archive-title' ).show();
        } );
    });

    // настройка
    wp.customize( 'color_scheme', function( value ) {
        value.bind( function( newVal ) {

                $( 'body' ).css({
                    'background-color': newVal,
                    'color'           : '#fff'
                });
        });
    });

    wp.customize('sidebar_position', function (value) {
        value.bind(function (newValue) {
            if (newValue == 'right')
            {
                 $('#sidebar-wrapper').css({'order':2});
            }if (newValue == 'left') {
                $('#sidebar-wrapper').css({'order':0});

            }

        })
    })
});