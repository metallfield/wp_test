$(document).ready(()=>{

  let val =  $('#is_archive').val();
    if (val === 'true')
    {
        $('#admin_post_types_filter').attr('hidden', false);
        $('#archive_posts_layout').attr('hidden', false);
    }
    if (val === 'false')
    {
        $('#admin_post_types_filter').attr('hidden', true);
        $('#archive_posts_layout').attr('hidden', true);
    }
    $('#is_archive').change(()=>{
       let value = $('#is_archive').val();
        if (value === 'true')
        {
            $('#admin_post_types_filter').attr('hidden', false);
            $('#archive_posts_layout').attr('hidden', false);
        }
        if (value === 'false')
        {
            $('#admin_post_types_filter').attr('hidden', true);
            $('#archive_posts_layout').attr('hidden', true);

        }
    });
})