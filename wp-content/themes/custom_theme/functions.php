<?php

function custom_styles_includes()
{
    wp_enqueue_style('slick-style', get_stylesheet_directory_uri().'/assets/css/slick.css');
     wp_enqueue_style('bootstrap', get_stylesheet_directory_uri().'/assets/css/bootstrap.min.css');
     wp_enqueue_style('custom-style', get_stylesheet_directory_uri().'/assets/css/custom-style.css');

}

add_action('wp_enqueue_scripts', 'custom_styles_includes');

function custom_scripts_includes()
{
    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', ( 'https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js' ), false, null, true );
    wp_enqueue_script( 'jquery' );
    wp_enqueue_script( 'bootstrap', get_stylesheet_directory_uri() . '/assets/js/bootstrap.bundle.min.js', array( 'jquery' ) );
    wp_register_script('slick-init', get_stylesheet_directory_uri().'/assets/js/slick-init.js', ['jquery']);
    wp_enqueue_script('slick-init');
    wp_enqueue_script('slick-script', get_stylesheet_directory_uri().'/assets/js/slick.min.js', ['jquery']);
    wp_enqueue_script('contact-form-sender', get_stylesheet_directory_uri().'/assets/js/contact-form-sender.js', ['jquery']);

}

add_theme_support('post-thumbnails', ['animals']);
add_action('wp_enqueue_scripts', 'custom_scripts_includes');


function include_custom_scripts_for_admin()
{
    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', ( 'https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js' ), false, null, true );
    wp_enqueue_script( 'jquery' );
    wp_enqueue_script('show-post-types-script', get_stylesheet_directory_uri().'/assets/js/show_post_types_for_archive_filter.js', ['jquery']);

}
add_action('admin_enqueue_scripts', 'include_custom_scripts_for_admin');

function custom_register_sidebars()
{
    $shared_args = array(
        'before_title'  => '<h2 class="widget-title subheading heading-size-3">',
        'after_title'   => '</h2>',
        'before_widget' => '<div class="widget %2$s"><div class="widget-content">',
        'after_widget'  => '</div></div>',
    );

    register_sidebar(
        array_merge(
            $shared_args,
            [
                'name' => __('My Custom Sidebar', 'wordpress.loc'),
                'id' => 'my_custom_sidebar',
            ]
        )
    );
}

add_action('init', 'custom_register_sidebars');


function register_custom_menu()
{
    $lacations = [
        'custom_menu' => __('Custom Menu', 'custom_theme')
    ];
    register_nav_menus($lacations);
}

add_action('init', 'register_custom_menu');
add_filter('wp_nav_menu', 'custom_menu_filter', 10, 2);

function custom_menu_filter($nav_menu, $args)
{
    if ($args->menu->name == 'custom menu')
    {
        return '<nav class="navbar navbar-expand-lg navbar-dark  bg-white p-2"><button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>'.$nav_menu.'</nav>';
    }
    return $nav_menu;
}


add_filter('wp_nav_menu_args', 'custom_menu_args', 10, 2);

function custom_menu_args($args)
{
    if ($args['theme_location'] == 'primary') {
        $args['container'] = 'div';
        $args['container_class'] = 'collapse navbar-collapse';
        $args['container_id'] = 'navbarCollapse';
        $args['menu_class'] = 'navbar-nav mr-auto';
        $args['link_before'] = '<li class="mx-2">';
        $args['link_after'] = '</li>';
    }
    return $args;
}

add_filter('wp_nav_menu', 'add_login_link', 10, 2);

function add_login_link($nav_menu, $args)
{
    if (!is_user_logged_in())
    {
         return $nav_menu.'<li class="ml-auto my-auto align-self-center text-info"><a href="'.wp_login_url().'" class="my-auto align-self-center text-center">login</a></li>';
    }
        return $nav_menu. '<li class="ml-auto text-info"><a href=" '.wp_logout_url().'">logout</a></li>';

}

add_filter('login_headerurl', 'change_login_header_url');
function change_login_header_url($login_header_url)
{

    $login_header_url = '/';
    return $login_header_url;
}

add_filter('login_redirect', 'change_login_redirect_url',10 ,3);
function change_login_redirect_url($redirect_to, $requested_redirect_to, $user)
{
    if ( isset( $user->roles ) && is_array( $user->roles ) ) {
        if ( in_array( 'administrator', $user->roles ) ) {
            return $redirect_to;
        }
        else {
            return  $_SERVER['HTTP_REFERER'] ?? home_url();
        }
    }
    return $redirect_to;

}

add_action('login_enqueue_scripts', 'hide_wordpress_logo', 10 ,2);

function hide_wordpress_logo()
{
    echo  '<style type="text/css">
    .login h1 a { display: none !important; }
</style>';
}


add_action('custom_slider_after_header', 'add_custom_slider_to_header', 30);
function add_custom_slider_to_header()
{
     if (get_post_meta(get_the_ID(), 'has_slider', true) == 1)
    {
        $args = [
            'post_type' => 'animals',
            'posts_per_page' => 5,
        ];
        $query = new WP_Query($args);
        //  var_dump($query->posts);
        echo '<div class="container-fluid h-full bg-white"
<div class="row h-full">
<div class="one-time h-full">';
        foreach ($query->posts as $post)
        {
            echo '<div class="h-full"><div class="w-100 h-full d-flex flex-column slider-wrapper" style="height: 100%; background-image: url('.get_the_post_thumbnail_url($post->ID). '); background-color: rgba(12,12,12,0.73); background-blend-mode: overlay">
<h3 class="text-center font-weight-bold text-capitalize text-white"  >' .$post->post_name.'</h3>
<p class="text-center font-weight-bold text-white" >'.$post->post_excerpt.'</p>
 </div></div>';
        }
        echo '</div></div></div>';
    }

}

include ('metaboxes/custom_metaboxes.php');

add_action('get_header', 'get_page_meta');

function get_page_meta()
{
     if ( get_post_meta(get_the_ID(), 'is_archive_page_template', true) == 'true')
    {
        global $wp_query;
        $args = [
          'post_type' => get_post_meta(get_the_ID(), 'archive_page_post_type', true),
//            'posts_per_page' => 3,
            'paged' => (get_query_var( 'paged' )) ?  get_query_var( 'paged' ) : 1,
        ];
         $query = new WP_Query($args);
        $wp_query = $query;
    }

}

include('customizer/customize_custom_settings.php');

require_once 'widgets/custom_widget.php';

add_action('custom_hello', 'custom_hello');
function custom_hello()
{
    return 'hello';
}
add_filter('the_content', 'add_custom_sidebar');

function add_custom_sidebar($content)
{
     if (is_page(get_the_ID()))
   {
       $output = '';
       ?> <style type="text/css"> .entry-content > *:not(.alignwide):not(.alignfull):not(.alignleft):not(.alignright):not(.is-style-wide){ max-width: 100%!important;}</style>
        <div class="container-fluid my-0"><div class="row my-0 justify-content-between">
              <div class="col-9 border border-dark   d-flex flex-column rounded order-1 bg-white  mx-2 my-0" id="content-wrapper">
                  <?php return $content ;
   }else{
       return $content;
   }

}

add_filter('the_content', 'insert_my_sidebar', 9);


function insert_my_sidebar($content)
{
    ob_start();?>
    </div>
    <div class="col bg-white border border-dark rounded p-2 " id="sidebar-wrapper">
        <div class="footer-widgets column-two grid-item ">
            <?php
    dynamic_sidebar('my_custom_sidebar');
    ?>  </div></div></div></div></div><?php
    $content .= ob_get_clean();

    return $content;
}

require_once 'blocks/test_block/custom_block.php';
require_once 'blocks/dynamyc_block/dynamyc_block.php';

add_action( 'phpmailer_init', 'custom_send_smtp_email' );


/**
 * @param PHPMailer $phpmailer
 */
function custom_send_smtp_email(PHPMailer\PHPMailer\PHPMailer $phpmailer) {
    $phpmailer->isSMTP();
    $phpmailer->Host       = 'smtp.gmail.com';
    $phpmailer->SMTPAuth   = true;
    $phpmailer->Port       = 465;
    $phpmailer->Username   = 'didjok6@gmail.com';
    $phpmailer->Password   = 'qawsed333777';
    $phpmailer->SMTPSecure = 'ssl';

}

//add_action('loop_end', 'test_mail');
//function test_mail()
//{
//     if (is_front_page())
//    {
//        var_dump('start');
//        $headers = "From: My Name <myname@mydomain.com>" . "\r\n";
//      $mail =  wp_mail('didjok6@gmail.com', 'Test wp message', 'message of test wp message');
//     var_dump($mail);
//    }
//}
//
//add_action( 'wp_mail_failed', 'onMailError', 10, 1 );
//function onMailError( $wp_error ) {
//    echo '<p class="error">'.$wp_error->get_error_message() . '</p>';
//}


include('shortcodes/custom_shortcodes.php');

