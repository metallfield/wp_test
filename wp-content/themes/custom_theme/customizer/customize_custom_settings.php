<?php
add_action('customize_register', 'customizer_init');
add_action( 'customize_preview_init', 'customizer_js_file' );
add_action( 'wp_head', 'customizer_style_tag' );
function customizer_init(WP_Customize_Manager $wp_customize)
{
    $transport = 'postMessage';

    if( $section = 'display_options' ) {

        $wp_customize->add_section($section, [
            'title' => 'Отображение',
            'priority' => 200,                   // приоритет расположения
            'description' => 'Внешний вид сайта', // описание не обязательное
        ]);

        // настройка
        $setting = 'display_header';

        $wp_customize->add_setting($setting, [
            'default' => 'true',
            'transport' => $transport
        ]);

        $wp_customize->add_control($setting, [
            'section' => $section,
            'label' => 'Отобразить заголовок?',
            'type' => 'checkbox',
        ]);

        // настройка
        $setting = 'color_scheme';

        $wp_customize->add_setting($setting, [
            'default' => 'normal',
            'transport' => $transport
        ]);

        $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize , $setting, [
            'section' => $section,
            'label' => 'Цветовая схема',
            'settings' => 'color_scheme'
        ]));

        // setting
        $setting = 'sidebar_position';

        $wp_customize->add_setting($setting, [
            'default' => 'right',
            'transport' => $transport
        ]);

        $wp_customize->add_control( $setting, [
            'section'  => $section,
            'label'    => __('Sidebar Position', 'myplugin_textdomain'),
            'type'     => 'radio',
            'settings' => 'sidebar_position',
            'choices'  => [
                'right'    => 'right',
                'left'   => 'left',
            ]
        ] );

        if ( isset( $wp_customize->selective_refresh ) ){

            $wp_customize->selective_refresh->add_partial( $setting, [
                'selector'            => '.site-footer .inner',
                'container_inclusive' => false,
                'render_callback'     => 'footer_inner_dh5theme',
                'fallback_refresh'    => false, // Prevents refresh loop when document does not contain .cta-wrap selector. This should be fixed in WP 4.7.
            ] );

            // поправим стиль, чтобы кнопку было видно
            add_action( 'wp_head', function(){
                echo '<style>.site-footer .inner .customize-partial-edit-shortcut{ margin: 10px 0 0 38px; }</style>';
            } );

        }

    }
}

function customizer_style_tag(){

    $style = [];

    $body_styles = [];
    $body_styles[] = 'background-color: '.get_theme_mod('color_scheme');

    $style[] = 'body { '. implode( ' ', $body_styles ) .' }';

    $style[] = 'a { color: ' . get_theme_mod( 'link_color' ) . '; }';
    if ( 'right' === get_theme_mod('sidebar_position'))
    {
        $style[] = '#sidebar-wrapper{ order: 2;}';
    }if ('left' === get_theme_mod('sidebar_position')){
        $style[] =  '#sidebar-wrapper{ order: 0;}';
    }
    if( ! get_theme_mod( 'display_header' ) )
        $style[] = '#archive-title { display: none; }';

    echo "<style>\n" . implode( "\n", $style ) . "\n</style>\n";
}

function customizer_js_file(){
    wp_enqueue_script( 'theme-customizer', get_stylesheet_directory_uri() . '/assets/js/theme-customizer.js', [ 'jquery', 'customize-preview' ], null, true );
}

