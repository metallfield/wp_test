<?php
/**
 * Plugin Name: Plugin For Animals
 */

add_action('init', 'register_custom_post_types');

function register_custom_post_types()
{
    register_post_type('guide', [
        'label'  => 'Guide',
        'labels' => [
            'name'               => 'Guides', // основное название для типа записи
            'singular_name'      => 'Guide', // название для одной записи этого типа
            'add_new'            => 'Добавить Guide', // для добавления новой записи
            'add_new_item'       => 'Добавление Guide', // заголовка у вновь создаваемой записи в админ-панели.
            'edit_item'          => 'Редактирование Guide', // для редактирования типа записи
            'new_item'           => 'Новое Guide', // текст новой записи
            'view_item'          => 'Смотреть Guide', // для просмотра записи этого типа.
            'search_items'       => 'Искать Guide', // для поиска по этим типам записи
            'not_found'          => 'Не найдено', // если в результате поиска ничего не было найдено
            'not_found_in_trash' => 'Не найдено в корзине', // если не было найдено в корзине
            'parent_item_colon'  => '', // для родителей (у древовидных типов)
            'menu_name'          => 'Guides', // название меню
        ],
        'description'         => '',
        'public'              => true,
        // 'publicly_queryable'  => null, // зависит от public
        // 'exclude_from_search' => null, // зависит от public
        // 'show_ui'             => null, // зависит от public
        // 'show_in_nav_menus'   => null, // зависит от public
        'show_in_menu'        => true, // показывать ли в меню адмнки
        'show_in_admin_bar'   => true, // зависит от show_in_menu
        'show_in_rest'        => true, // добавить в REST API. C WP 4.7
        'rest_base'           => null, // $post_type. C WP 4.7
        'menu_position'       => null,
        'menu_icon'           => null,
        //'capability_type'   => 'post',
        //'capabilities'      => 'post', // массив дополнительных прав для этого типа записи
        //'map_meta_cap'      => null, // Ставим true чтобы включить дефолтный обработчик специальных прав
        'hierarchical'        => false,
        'supports'            => [
            'title',
            'editor',
            'author','
            thumbnail',
            'excerpt',
            'trackbacks',
            'custom-fields',
            'comments',
            'revisions',
            'page-attributes',
            'post-formats' ], // 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
        'has_archive'         => true,
        'taxonomies'          => ['guide_type'],
        'rewrite'             => true,
        'query_var'           => true,
    ]);
    register_post_type('animals', [
        'label'  => 'Guide',
        'labels' => [
            'name'               => 'Animals', // основное название для типа записи
            'singular_name'      => 'Animal', // название для одной записи этого типа
            'add_new'            => 'Добавить Animal', // для добавления новой записи
            'add_new_item'       => 'Добавление Animal', // заголовка у вновь создаваемой записи в админ-панели.
            'edit_item'          => 'Редактирование Animal', // для редактирования типа записи
            'new_item'           => 'Новое Animal', // текст новой записи
            'view_item'          => 'Смотреть Animal', // для просмотра записи этого типа.
            'search_items'       => 'Искать Animal', // для поиска по этим типам записи
            'not_found'          => 'Не найдено', // если в результате поиска ничего не было найдено
            'not_found_in_trash' => 'Не найдено в корзине', // если не было найдено в корзине
            'parent_item_colon'  => '', // для родителей (у древовидных типов)
            'menu_name'          => 'Animals', // название меню
        ],
        'description'         => '',
        'public'              => true,
        // 'publicly_queryable'  => null, // зависит от public
        // 'exclude_from_search' => null, // зависит от public
        // 'show_ui'             => null, // зависит от public
        // 'show_in_nav_menus'   => null, // зависит от public
        'show_in_menu'        => true, // показывать ли в меню адмнки
        'show_in_admin_bar'   => true, // зависит от show_in_menu
        'show_in_rest'        => true, // добавить в REST API. C WP 4.7
        'rest_base'           => null, // $post_type. C WP 4.7
        'menu_position'       => null,
        'menu_icon'           => null,
        //'capability_type'   => 'post',
        //'capabilities'      => 'post', // массив дополнительных прав для этого типа записи
        //'map_meta_cap'      => null, // Ставим true чтобы включить дефолтный обработчик специальных прав
        'hierarchical'        => false,
        'supports'            => [
            'title',
            'editor',
            'author',
            'thumbnail',
            'excerpt',
            'trackbacks',
            'custom-fields',
            'comments',
            'revisions',
            'page-attributes',
            'post-formats' ], // 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
        'has_archive'         => false,
        'taxonomies'          => ['animal_type'],
        'rewrite'             => true,
        'query_var'           => true,
    ]);
}

add_action('init', 'register_custom_taxonomies');

function register_custom_taxonomies()
{
    register_taxonomy('animal_type', ['animals'], [
        'label'                 => 'Animals Type', // определяется параметром $labels->name
        'labels'                => [
            'name'              => 'Animals Types',
            'singular_name'     => 'Animals Type',
            'search_items'      => 'Search Animals Type',
            'all_items'         => 'All Animals Types',
            'view_item '        => 'View Animals Type',
            'parent_item'       => 'Parent Animals Type',
            'parent_item_colon' => 'Parent Animals Type:',
            'edit_item'         => 'Edit Animals Type',
            'update_item'       => 'Update Animals Type',
            'add_new_item'      => 'Add New Animals Type',
            'new_item_name'     => 'New Animals Type Name',
            'menu_name'         => 'Animals Type',
        ],
        'description'           => '', // описание таксономии
        'public'                => true,
        // 'publicly_queryable'    => null, // равен аргументу public
        // 'show_in_nav_menus'     => true, // равен аргументу public
        // 'show_ui'               => true, // равен аргументу public
        // 'show_in_menu'          => true, // равен аргументу show_ui
        // 'show_tagcloud'         => true, // равен аргументу show_ui
        // 'show_in_quick_edit'    => null, // равен аргументу show_ui
        'hierarchical'          => true,

        'rewrite'               => true,
        //'query_var'             => $taxonomy, // название параметра запроса
        'capabilities'          => array(),
        'meta_box_cb'           => null, // html метабокса. callback: `post_categories_meta_box` или `post_tags_meta_box`. false — метабокс отключен.
        'show_admin_column'     => true, // авто-создание колонки таксы в таблице ассоциированного типа записи. (с версии 3.5)
        'show_in_rest'          => true, // добавить в REST API
        'rest_base'             => null, // $taxonomy
        // '_builtin'              => false,
        //'update_count_callback' => '_update_post_term_count',
    ]);

}

add_action('get_header', 'test');

function test()
{
     if (get_post_meta(get_the_ID(), 'is_archive_page_template', true) == 'true')
    {
        add_action('loop_start', 'change_archive_animals_loop_markdown');

        function change_archive_animals_loop_markdown($query)
        {
            if ( $query->post->post_type == 'animals')
            {
                echo ' 	<header class="archive-header has-text-align-center header-footer-group" id="archive-title">
			<div class="archive-header-inner section-inner medium">   
				<h1 class="archive-title text-danger">'.get_the_title(get_the_ID()).' </h1>
			</div>
			</header>
			<div class="row my-2 p-2 justify-content-around flex-wrap">';
                echo '<style type="text/css">
               hr.post-separator{ display: none!important;}</style>';
            }
        }

        add_action('loop_end', 'close_archive_animals_loop_markdown');

        function close_archive_animals_loop_markdown($query)
        {
            if ($query->post->post_type == 'animals')
            {
                echo '</div>';
                get_template_part( 'template-parts/pagination' );
            }

        }
        $layout = get_post_meta(get_the_ID(), 'archive_posts_layout', true);
        if ($layout == 'line')
        {
            add_filter('post_class', 'filter_archive_layouts_line', 10, 3);
            function filter_archive_layouts_line($classes, $class, $post_id)
            {
                $classes[] = 'container mx-2 my-2 border border-dark p-2 rounded bg-white text-center justify-content-center';
                return $classes;
            }
        }
        if ($layout == 'grid')
        {
            add_filter('post_class', 'filter_archive_layouts_grid', 10, 3);
            function filter_archive_layouts_grid($classes, $class, $post_id)
            {
                $classes[] = 'col-3 mx-2 my-2 border border-dark p-2 rounded bg-white ';
                return $classes;
            }
        }

        add_filter('the_content', 'filter_guides_archive_content');

        function filter_guides_archive_content()
        {
            if ($GLOBALS['post']->post_type == 'animals'
                && !empty($GLOBALS['post']->post_excerpt)
            )
            {
                 $GLOBALS['post']->post_content = $GLOBALS['post']->post_excerpt;
            }
            return $GLOBALS['post']->post_content;
        }
    }
}


add_action('the_content', 'add_custom_taxonomies_to_animals', 10,2);
function add_custom_taxonomies_to_animals($content)
{
    global $wp_query;

    if ($wp_query->post && $wp_query->post->post_type == 'animals')
    {
       return '<div class="mb-auto">' .the_terms(get_the_ID(),
               'animal_type',
               '<div class="mb-auto font-weight-bold "> ',
               ' , ',
               '</div><br>').'</div><br>'.$content.'</div>';

    }
    return  $content;
}




