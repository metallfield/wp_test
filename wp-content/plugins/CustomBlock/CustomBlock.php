<?php
/**
 * Plugin Name: Custom Block
 */


add_action('init', 'init_custom_class');

function init_custom_class()
{
    new Gutenberg_Block_Example(); // инициализация
}

class Gutenberg_Block_Example
{

    public function __construct()
    {
        add_action('init', [$this, 'gutenberg_block_register_block']);
        add_action('enqueue_block_editor_assets', [$this, 'gutenberg_block_editor_scripts']);

    }

    public function gutenberg_block_register_block()
    {
        register_block_type('custom-block/example', [
            'attributes' => [
                'postsPerPage' => [
                    'type' => 'number',
                    'default' => 3,
                ],
                'order' => [
                    'type' => 'string',
                    'default' => 'desc',
                ],
            ],
         ]);
    }

    public function gutenberg_block_editor_scripts()
    {
        wp_enqueue_script('google-map-script', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyA3tD5rPn0Tkd8ool8kd1lezWCL72DdWbs&libraries=places&callback=initialize', ['google-map-init'], null, true );
        wp_enqueue_script('google-map-init', get_stylesheet_directory_uri().'/assets/js/google-maps-init.js', ['jquery', 'example'], null, true);
        wp_register_script(
            'example',
            plugins_url( 'build/index.js', __FILE__ ),
            ['wp-blocks', 'wp-element', 'wp-editor']
        );

        wp_enqueue_script('example');

    }


}