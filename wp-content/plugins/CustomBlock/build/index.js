( function(blocks, editor, data ) {
   var  withSelect = data.withSelect;

    blocks.registerBlockType( 'custom-block/example', {
        title: 'Google Map Custom Block',
        icon: 'map-access-alt',
        category: 'widgets',
        attributes: {
            content: {},
            lat: {},
            lng: {},
            color: {}
        },
        example: {},
        edit:function(props) {
             function updateContent(event) {
                props.setAttributes({content: event.target.value})
            }
            function updateColor(value) {
                props.setAttributes({color: value.hex})
            }
            function updateLatitude(event) {
                props.setAttributes({lat: event.target.value});
            }
            function updateLongitude(event) {
                props.setAttributes({lng: event.target.value});
            }
            return React.createElement(
                "div",
                {style: {display: 'flex'}},

                wp.element.createElement(
                    'div',
                    null,
                    React.createElement(
                        "h4",
                        null,
                        "Enter Latitude"
                    ),
                    React.createElement("input", { type: "text", value: props.attributes.lat, onChange: updateLatitude }),
                ),
               wp.element.createElement(
                   'div',
                   null,
                   React.createElement(
                       "h4",
                       null,
                       "Enter Longitude"
                   ),
                   React.createElement("input", { type: "text", value: props.attributes.lng, onChange: updateLongitude }),
               ),
                React.createElement(
                    'div',
                    {},
                    wp.element.createElement(
                        "div",
                        {   class: 'form-group' },
                        wp.element.createElement(
                            'input',
                            {
                                type: 'text',
                                id: 'address-input',
                                hidden: true,
                                name: 'address_address',
                                class: 'form-control map-input',

                            },
                        ),
                        wp.element.createElement(
                            'input',
                            {
                                type: 'hidden',
                                name: 'address_latitude',
                                id: 'address-latitude',
                                value: props.attributes.lat,
                            }
                        ),
                        wp.element.createElement(
                            'input',
                            {
                                type: 'hidden',
                                name: 'address_longitude',
                                id: 'address-longitude',
                                value: props.attributes.lng,
                            }
                        ),
                    ),
                    React.createElement(
                        'div',
                        {
                            id: 'address-map-container',
                         },
                        React.createElement(
                            'div',
                            {
                                 id: 'address-map',
                                name: 'address-map',
                            }
                        )
                    )
                )
             );
        },
        save: function(props) {
             return wp.element.createElement(
                'div',
                {},
                wp.element.createElement(
                    "div",
                    {   class: 'form-group' },
                    wp.element.createElement(
                        'input',
                        {
                            type: 'text',
                            id: 'address-input',
                            hidden: true,
                            name: 'address_address',
                            class: 'form-control map-input',
                            value: ''
                        },
                    ),
                    wp.element.createElement(
                        'input',
                        {
                            type: 'hidden',
                            name: 'address_latitude',
                            id: 'address-latitude',
                            value: props.attributes.lat,
                        }
                    ),
                    wp.element.createElement(
                        'input',
                        {
                            type: 'hidden',
                            name: 'address_longitude',
                            id: 'address-longitude',
                            value: props.attributes.lng,
                        }
                    ),
                ),
                wp.element.createElement(
                    'div',
                    {
                        id: 'address-map-container',
                        style: 'width:100%; height:400px;'
                    },
                    wp.element.createElement(
                        'div',
                        {
                            style: 'width: 100%; height: 100%',
                            id: 'address-map'
                        }
                    )
            )

        )
        }
    } );
}(
    window.wp.blocks,
    window.wp.editor,
    window.wp.data,
 ) );
