<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define( 'DB_NAME', 'wp_test' );

/** Имя пользователя MySQL */
define( 'DB_USER', 'dev' );

/** Пароль к базе данных MySQL */
define( 'DB_PASSWORD', 'dev' );

/** Имя сервера MySQL */
define( 'DB_HOST', 'localhost' );

/** Кодировка базы данных для создания таблиц. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Схема сопоставления. Не меняйте, если не уверены. */
define( 'DB_COLLATE', '' );

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '0><I8~)+!pAd^[M|WZ3~E1>0yAeXa{%=0u_5f=7vM)G@PNZ?$)@di(l2[6.RTTxL' );
define( 'SECURE_AUTH_KEY',  '.u6S#L`4xdWG#S+~vC,r[OYCwWoZA=BP7XWw!>[h~igI_^e2pb:|=oRn[zF&Xk$ ' );
define( 'LOGGED_IN_KEY',    'BME?!v2jLG,q*i4r(.[juMt`^_IEJv%AtF@%c(Nj[3-.m{<Xg~7,j91x,vyP7Mh@' );
define( 'NONCE_KEY',        ']4BU3.SRfH4v2#h7XoN[9IybD_TQrAs|[.z1`oJI8?kE#O^|l[F|31;(ORcU~*Xu' );
define( 'AUTH_SALT',        'c TXf$T5?wZiJ|QjIdeV[r9}c,t3KmDedr9+:yueQgO?eaWRu9KaZGYz63svZqK ' );
define( 'SECURE_AUTH_SALT', '@2o;vBR3$OZG^b4haMmQDG.apm1K]5&fEOf3# W]OI(|@p&IE4K6vQx(j5Do?zi?' );
define( 'LOGGED_IN_SALT',   'z[5MRPA;JGrMVr3v26V t)I,.;^3`G~(YF&{2se#74vurDn<q+3+HDJQ^SdH>2Jl' );
define( 'NONCE_SALT',       'K(@{))]a|dTCpi|=p cNKg8m8T=OG%ICd Tp6j1BS+x(HKKD1fV2Q}_4,tpxlt&f' );

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', true );

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Инициализирует переменные WordPress и подключает файлы. */
require_once( ABSPATH . 'wp-settings.php' );
